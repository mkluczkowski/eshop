﻿using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Options;
using Shop.Application.Abstractions.Database;

namespace Shop.Persistence.Database;

internal sealed class SqlConnectionFactory : ISqlConnectionFactory
{
    private readonly ConnectionStringsOptions _options;

    public SqlConnectionFactory(IOptions<ConnectionStringsOptions> options)
    {
        _options = options.Value;
    }

    public SqlConnection CreateConnection()
    {
        var connection = new SqlConnection(_options.Database);

        return connection;
    }
}
