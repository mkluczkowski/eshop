﻿using Dapper.Transaction;
using Shop.Application.Abstractions.Database;
using Shop.Domain.Products;
using Shop.Domain.Products.Abstractions;
using static Dapper.SqlMapper;

namespace Shop.Persistence.Repositories;

public class ProductsRepository : IProductsRepository
{
    private readonly ISqlConnectionFactory _sqlConnectionFactory;

    public ProductsRepository(ISqlConnectionFactory sqlConnectionFactory)
    {
        _sqlConnectionFactory = sqlConnectionFactory;
    }

    public async Task<IEnumerable<Product>> GetAll()
    {
        using var database = _sqlConnectionFactory.CreateConnection();

        string sql = $@"SELECT Id, Name, Sku, Currency, Amount 
                        FROM Products";

        var result = await database.QueryAsync<Product>(sql);

        return result;
    }

    public async Task<Product> GetById(Guid id)
    {
        using var database = _sqlConnectionFactory.CreateConnection();

        string sql = $@"SELECT Id, Name, Sku, Currency, Amount 
                        FROM Products 
                        WHERE Id = @Id";

        var result = await database.QuerySingleOrDefaultAsync<Product>(sql, new { Id = id });

        return result;
    }

    public async Task Create(Product product)
    {
        using var database = _sqlConnectionFactory.CreateConnection();

        database.Open();

        using var transaction = database.BeginTransaction();

        string sql = $@"INSERT INTO Products 
                        (Id, Name, Sku, Currency, Amount)
                        VALUES 
                        (@Id, @Name, @Sku, @Currency, @Amount)";

        try
        {
            await transaction.ExecuteAsync(sql, new
            {
                product.Id,
                product.Name,
                product.Sku?.Value,
                product.Price?.Currency,
                product.Price?.Amount,
            });

            transaction.Commit();
        }
        catch (Exception)
        {
            transaction.Rollback();
        }
    }

    public async Task Update(Product entity)
    {
        using var database = _sqlConnectionFactory.CreateConnection();

        string sql = $@"UPDATE Products 
                        SET Id = @Id, Name = @Name, Sku = @Sku, Currency = @Currency, Amount = @Amount
                        WHERE Id = @Id";

        await database.ExecuteAsync(sql, entity);
    }

    public async Task Delete(Guid id)
    {
        using var database = _sqlConnectionFactory.CreateConnection();

        string sql = $"DELETE FROM Products WHERE Id = @Id";

        await database.ExecuteAsync(sql, new { Id = id });
    }
}
