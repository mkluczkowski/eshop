﻿using Dapper;
using Dapper.Transaction;
using Shop.Application.Abstractions.Database;
using Shop.Domain.Customers;
using Shop.Domain.Customers.Abstractions;
using System.Security.Cryptography;
using System.Text;
using static Dapper.SqlMapper;

namespace Shop.Persistence.Repositories;

public sealed class CustomersRepository : ICustomersRepository
{
    private readonly ISqlConnectionFactory _sqlConnectionFactory;

    public CustomersRepository(ISqlConnectionFactory sqlConnectionFactory)
    {
        _sqlConnectionFactory = sqlConnectionFactory;
    }

    public async Task<IEnumerable<Customer?>> GetAll()
    {
        using var database = _sqlConnectionFactory.CreateConnection();

        string sql = $"SELECT Id, Name, Email FROM Customers";

        var result = await database.QueryAsync<Customer>(sql);

        return result;
    }

    public async Task<Customer?> GetById(Guid id)
    {
        using var database = _sqlConnectionFactory.CreateConnection();

        string sql = $@"SELECT Id, Name, Email 
                        FROM Customers 
                        WHERE Id = @Id";

        var result = await database.QuerySingleOrDefaultAsync<Customer>(sql, new { Id = id });

        return result;
    }

    public async Task<Customer?> GetCustomerByLoginCredentials(string Email, string Password)
    {
        string sql = @"SELECT Id, Name, Email 
                        FROM Customers 
                        WHERE Email = @Email
                        AND Password = @Password;";

        var hashedPassword = HashString(Password);

        using var database = _sqlConnectionFactory.CreateConnection();

        Customer result = await database.QueryFirstOrDefaultAsync<Customer>(sql, new
        {
            Email,
            Password = hashedPassword
        });

        return result;
    }

    public async Task<long> GetCustomerCount()
    {
        string sql = @"SELECT COUNT(Id)
                        FROM Customers";

        using var database = _sqlConnectionFactory.CreateConnection();

        var result = await database.QueryFirstAsync<long>(sql);

        return result;
    }

    public async Task Create(Customer entity)
    {
        using var database = _sqlConnectionFactory.CreateConnection();

        database.Open();

        using var transaction = database.BeginTransaction();

        string sql = $@"INSERT INTO Customers 
                        (Id, Name, Email, Password)
                        VALUES 
                        (@Id, @Name, @Email, @Password)";

        try
        {
            await transaction.ExecuteAsync(sql, entity);

            transaction.Commit();
        }
        catch (Exception)
        {

            transaction.Rollback();
        }
    }

    public async Task Update(Customer entity)
    {
        using var database = _sqlConnectionFactory.CreateConnection();

        database.Open();

        using var transaction = database.BeginTransaction();

        string sql = $@"UPDATE Customers 
                        SET Id = @Id, Name = @Name, Email = @Emial
                        WHERE Id = @Id";

        try
        {
            await transaction.ExecuteAsync(sql, entity);

            transaction.Commit();
        }
        catch (Exception)
        {

            transaction.Rollback();
        }
    }

    public async Task Delete(Guid id)
    {
        using var database = _sqlConnectionFactory.CreateConnection();

        database.Open();

        using var transaction = database.BeginTransaction();

        string sql = $@"DELETE FROM Customers 
                        WHERE Id = @Id";

        try
        {
            await transaction.ExecuteAsync(sql, new { Id = id });

            transaction.Commit();
        }
        catch (Exception)
        {

            transaction.Rollback();
        }
    }

    private static string HashString(string input)
    {
        byte[] hash = SHA256.HashData(Encoding.UTF8.GetBytes(input));
        var hashedString = Convert.ToHexString(hash);

        return hashedString;
    }
}
