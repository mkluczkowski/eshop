using Carter;
using FluentAssertions;
using MediatR;
using NetArchTest.Rules;
using System.Windows.Input;

namespace Shop.Architecture.Tests
{
    public class ArchitectureTests
    {
        private const string DomainNamespace = "Shop.Domain";
        private const string ApplicationNamespace = "Shop.Application";
        private const string InfrastructureNamespace = "Shop.Infrastructure";
        private const string PersistanceNamespace = "Shop.Presistance";
        private const string PresentationNamespace = "Shop.Presentation";
        private const string ApiNamespace = "Shop";

        #region Domain Tests

        [Fact]
        public void Domain_Should_Not_HaveDependencyOnOtherProjects()
        {
            //Arrange
            var assembly = typeof(Domain.AssemblyReference).Assembly;

            var otherPorjects = new[]
            {
                ApplicationNamespace,
                InfrastructureNamespace,
                PersistanceNamespace,
                PresentationNamespace,
                ApiNamespace
            };

            //Act
            var testResult = Types
                .InAssembly(assembly)
                .ShouldNot()
                .HaveDependencyOnAll(otherPorjects)
                .GetResult();

            //Assert
            testResult.IsSuccessful.Should().BeTrue();
        }

        [Fact]
        public void Domain_Interfaces_Should_BePublic()
        {
            //Arrange
            var assembly = typeof(Domain.AssemblyReference).Assembly;

            //Act
            var testResult = Types
                .InAssembly(assembly)
                .That()
                .AreInterfaces()
                .Should()
                .BePublic()
                .GetResult();

            //Assert
            testResult.IsSuccessful.Should().BeTrue();
        }

        #endregion

        #region Application Tests

        [Fact]
        public void Application_Should_Not_HaveDependencyOnOtherProjects()
        {
            //Arrange
            var assembly = typeof(Application.AssemblyReference).Assembly;

            var otherProjects = new[]
            {
                InfrastructureNamespace,
                PresentationNamespace,
                ApiNamespace
            };

            //Act
            var testResult = Types
                .InAssembly(assembly)
                .ShouldNot()
                .HaveDependencyOnAll(otherProjects)
                .GetResult();

            //Assert
            testResult.IsSuccessful.Should().BeTrue();
        }

        [Fact]
        public void Handlers_Should_HaveDependencyOnDomain()
        {
            //Arrange
            var assembly = typeof(Application.AssemblyReference).Assembly;

            //Act
            var testResult = Types
                .InAssembly(assembly)
                .That()
                .HaveNameEndingWith("Handler")
                .Should()
                .HaveDependencyOn(DomainNamespace)
                .GetResult();

            //Assert
            testResult.IsSuccessful.Should().BeTrue();
        }

        [Fact]
        public void Handlers_Should_BeSealed()
        {
            //Arrange
            var assembly = typeof(Application.AssemblyReference).Assembly;

            //Act
            var testResult = Types
                .InAssembly(assembly)
                .That()
                .HaveNameEndingWith("Handlers")
                .Should()
                .NotBePublic()
                .And()
                .BeSealed()
                .GetResult();

            //Assert
            testResult.IsSuccessful.Should().BeTrue();
        }

        [Fact]
        public void Handlers_Should_ImplementIRequestHandlerInterface()
        {
            //Arrange
            var assembly = typeof(Application.AssemblyReference).Assembly;

            //Act
            var testResult = Types
                .InAssembly(assembly)
                .That()
                .HaveNameEndingWith("Handler")
                .And()
                .DoNotHaveNameStartingWith("CacheInvalidation")
                .Should()
                .ImplementInterface(typeof(IRequestHandler<>))
                .Or()
                .ImplementInterface(typeof(IRequestHandler<,>))
                .GetResult();

            //Assert
            testResult.IsSuccessful.Should().BeTrue();
        }

        [Fact]
        public void Commands_Should_ImplementIRequestInterface()
        {
            //Arrange
            var assembly = typeof(Application.AssemblyReference).Assembly;

            //Act
            var testResult = Types
                .InAssembly(assembly)
                .That()
                .HaveNameEndingWith("Command")
                .Should()
                .ImplementInterface(typeof(IRequest<>))
                .Or()
                .ImplementInterface(typeof(IRequest))
                .GetResult();

            //Assert
            testResult.IsSuccessful.Should().BeTrue();
        }

        #endregion

        #region Infrastructure Tests

        [Fact]
        public void Infrastructure_Should_Not_HaveDependencyOnOtherProjects()
        {
            //Arrange
            var assembly = typeof(Application.AssemblyReference).Assembly;

            var otherProjects = new[]
            {
                PresentationNamespace,
                ApiNamespace
            };
            //Act
            var testResult = Types
                .InAssembly(assembly)
                .ShouldNot()
                .HaveDependencyOnAll(otherProjects)
                .GetResult();

            //Assert
            testResult.IsSuccessful.Should().BeTrue();
        }

        #endregion

        #region Presentation Tests

        [Fact]
        public void Presentation_Should_Not_HaveDependencyOnOtherProjects()
        {
            //Arrange
            var assembly = typeof(Application.AssemblyReference).Assembly;

            var otherProjects = new[]
            {
                InfrastructureNamespace,
                ApiNamespace
            };

            //Act
            var testResult = Types
                .InAssembly(assembly)
                .ShouldNot()
                .HaveDependencyOnAll(otherProjects)
                .GetResult();

            //Assert
            testResult.IsSuccessful.Should().BeTrue();
        }

        [Fact]
        public void Modules_Should_HaveDependencyOnCarter()
        {
            //Arrange
            var assembly = typeof(Presentation.AssemblyReference).Assembly;

            //Act
            var testResult = Types
                .InAssembly(assembly)
                .That()
                .HaveNameEndingWith("Module")
                .Should()
                .HaveDependencyOn("Carter")
                .GetResult();

            //Assert
            testResult.IsSuccessful.Should().BeTrue();
        }

        [Fact]
        public void Modules_Should_HaveDependencyOnMediatR()
        {
            //Arrange
            var assembly = typeof(Presentation.AssemblyReference).Assembly;

            //Act
            var testResult = Types
                .InAssembly(assembly)
                .That()
                .HaveNameEndingWith("Module")
                .Should()
                .HaveDependencyOn("MediatR")
                .GetResult();

            //Assert
            testResult.IsSuccessful.Should().BeTrue();
        }

        [Fact]
        public void Modules_Should_BeInheritingICarterModule()
        {
            //Arrange
            var assembly = typeof(Presentation.AssemblyReference).Assembly;

            //Act
            var testResult = Types
                .InAssembly(assembly)
                .That()
                .Inherit(typeof(ICarterModule))
                .Should()
                .HaveNameEndingWith("Module")
                .GetResult();

            //Assert
            testResult.IsSuccessful.Should().BeTrue();
        }

        #endregion
    }
}