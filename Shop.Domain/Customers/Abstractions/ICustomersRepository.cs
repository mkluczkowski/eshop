﻿namespace Shop.Domain.Customers.Abstractions;

public interface ICustomersRepository
{
    Task<IEnumerable<Customer>> GetAll();
    Task<Customer?> GetById(Guid id);
    Task<Customer?> GetCustomerByLoginCredentials(string Name, string Password);
    Task<long> GetCustomerCount();

    Task Create(Customer entity);
    Task Update(Customer entity);
    Task Delete(Guid id);
}
