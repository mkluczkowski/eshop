﻿namespace Shop.Domain.Products;

public class Product
{
    public Guid Id { get; set; }
    public string Name { get; set; } = string.Empty;
    public Money? Price { get; set; }
    public Sku? Sku { get; set; }
}
