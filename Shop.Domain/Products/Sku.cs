﻿namespace Shop.Domain.Products;

public record Sku
{
    private Sku(string value) => Value = value;

    public string Value { get; init; }

    public static Sku? Create(string value) => new(value);
}
