﻿namespace Shop.Domain.Products.Abstractions;

public interface IProductsRepository
{
    Task<IEnumerable<Product>> GetAll();
    Task<Product> GetById(Guid id);

    Task Create(Product entity);
    Task Update(Product entity);
    Task Delete(Guid id);
}
