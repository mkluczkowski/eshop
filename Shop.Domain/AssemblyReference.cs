﻿using System.Reflection;

namespace Shop.Domain;

public static class AssemblyReference
{
    public static Assembly Assembly = typeof(AssemblyReference).Assembly;
}
