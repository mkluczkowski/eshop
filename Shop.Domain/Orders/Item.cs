﻿using Shop.Domain.Products;

namespace Shop.Domain.Orders;

public class Item
{
    internal Item(Guid id, long orderId, long productId, Money price, int productCount)
    {
        Id = id;
        OrderId = orderId;
        ProductId = productId;
        Price = price;
        ProductCount = productCount;
    }

    private Item()
    {

    }

    public Guid Id { get; private set; }
    public long OrderId { get; private set; }
    public long ProductId { get; private set; }
    public Money? Price { get; private set; }
    public int ProductCount { get; private set; }
}