﻿namespace Shop.Domain.Orders;

public class Order
{
    public long Id { get; private set; }

    public long CustomerId { get; private set; }

    public IReadOnlyList<Item>? Items { get; private set; }
}
