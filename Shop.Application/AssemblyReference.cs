﻿using System.Reflection;

namespace Shop.Application;

public class AssemblyReference
{
    public static Assembly Assembly = typeof(AssemblyReference).Assembly;
}
