﻿using Microsoft.Data.SqlClient;

namespace Shop.Application.Abstractions.Database;

public interface ISqlConnectionFactory
{
    SqlConnection CreateConnection();
}
