﻿using Shop.Domain.Customers;

namespace Shop.Application.Abstractions.Authentication;

public interface IJsonWebTokenProvider
{
    string Generate(Customer customer);
}
