﻿using MediatR;
using Shop.Domain.Products;
using Shop.Domain.Products.Abstractions;

namespace Shop.Application.Products.Create;

internal sealed class CreateProductCommandHandler : IRequestHandler<CreateProductCommand, CreateProductResponse>
{
    private readonly IProductsRepository _productsRepository;
    private readonly IPublisher _publisher;

    public CreateProductCommandHandler(IProductsRepository productsRepository, IPublisher publisher)
    {
        _productsRepository = productsRepository;
        _publisher = publisher;
    }

    public async Task<CreateProductResponse> Handle(CreateProductCommand request, CancellationToken cancellationToken)
    {
        Product product = new()
        { 
            Id = Guid.NewGuid(), 
            Name = request.Name, 
            Sku = Sku.Create(request.Sku), 
            Price = new Money(request.Currency, request.Amount)
        };

        await _productsRepository.Create(product);

        await _publisher.Publish(
            new ProductCreatedEvent 
            { 
                Id = product.Id 
            }, 
            cancellationToken);

        var response = new CreateProductResponse(product.Id);

        return response;
    }
}
