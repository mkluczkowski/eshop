﻿namespace Shop.Application.Products.Create;

public record CreateProductRequest(
    string Name,
    string Sku,
    string Currency,
    decimal Amount);
