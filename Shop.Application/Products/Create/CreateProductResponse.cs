﻿namespace Shop.Application.Products.Create;

public record CreateProductResponse(Guid Id);
