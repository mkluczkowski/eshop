﻿using MediatR;

namespace Shop.Application.Products.Create;

public record ProductCreatedEvent : INotification
{
    public Guid Id { get; init; }
}
