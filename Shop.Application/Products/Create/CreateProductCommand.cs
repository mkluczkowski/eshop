﻿using MediatR;

namespace Shop.Application.Products.Create;

public record CreateProductCommand(string Name, string Sku, string Currency, decimal Amount) : IRequest<CreateProductResponse>;
