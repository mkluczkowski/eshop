﻿using MediatR;
using Shop.Domain.Products.Abstractions;

namespace Shop.Application.Products.Delete;

internal sealed class DeleteProductCommandHandler : IRequestHandler<DeleteProductCommand>
{
    private readonly IProductsRepository _productsRepository;
    private readonly IPublisher _publisher;

    public DeleteProductCommandHandler(IProductsRepository productsRepository, IPublisher publisher)
    {
        _productsRepository = productsRepository;
        _publisher = publisher;
    }

    public async Task Handle(DeleteProductCommand request, CancellationToken cancellationToken)
    {
        await _productsRepository.Delete(request.Id);

        await _publisher.Publish(new ProductDeletedEvent { Id = request.Id }, cancellationToken);   
    }
}
