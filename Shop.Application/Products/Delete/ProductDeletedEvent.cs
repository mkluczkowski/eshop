﻿using MediatR;

namespace Shop.Application.Products.Delete;

public record ProductDeletedEvent : INotification
{
    public Guid Id { get; init; }
}
