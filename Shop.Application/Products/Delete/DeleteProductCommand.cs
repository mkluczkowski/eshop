﻿using MediatR;

namespace Shop.Application.Products.Delete;

public record DeleteProductCommand(Guid Id) : IRequest;
