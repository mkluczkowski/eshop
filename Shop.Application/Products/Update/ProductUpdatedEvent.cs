﻿using MediatR;

namespace Shop.Application.Products.Update;

public record ProductUpdatedEvent : INotification
{
    public Guid Id { get; init; }
}
