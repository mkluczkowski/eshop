﻿using MediatR;

namespace Shop.Application.Products.Update;

public record UpdateProductCommand(
    Guid Id,
    string Name,
    string Sku,
    string Currency,
    decimal Amount) : IRequest;