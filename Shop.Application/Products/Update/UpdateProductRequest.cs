﻿namespace Shop.Application.Products.Update;

public record UpdateProductRequest(
    Guid Id, 
    string Name,
    string Sku,
    string Currency,
    decimal Amount);
