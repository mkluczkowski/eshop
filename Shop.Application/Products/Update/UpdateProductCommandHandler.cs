﻿using MediatR;
using Shop.Domain.Products;
using Shop.Domain.Products.Abstractions;

namespace Shop.Application.Products.Update;

internal sealed class UpdateProductCommandHandler : IRequestHandler<UpdateProductCommand>
{
    private readonly IProductsRepository _productsRepository;
    private readonly IPublisher _publisher;

    public UpdateProductCommandHandler(IProductsRepository productsRepository, IPublisher publisher)
    {
        _productsRepository = productsRepository;
        _publisher = publisher;
    }

    public async Task Handle(UpdateProductCommand request, CancellationToken cancellationToken)
    {
        Product product = new()
        {
            Id = request.Id,
            Name = request.Name,
            Sku = Sku.Create(request.Sku),
            Price = new Money(request.Currency, request.Amount)
        };

        await _productsRepository.Update(product);

        await _publisher.Publish(new ProductUpdatedEvent { Id = product.Id }, cancellationToken);
    }
}
