﻿using MediatR;
using Shop.Application.Abstractions.Caching;
using Shop.Domain.Products;
using Shop.Domain.Products.Abstractions;

namespace Shop.Application.Products.GetProductById;

internal sealed class GetProductByIdQueryHandler : IRequestHandler<GetProducyByIdQuery, GetProductByIdResponse>
{
    private readonly IProductsRepository _productsRepository;
    private readonly ICacheService _cacheService;

    public GetProductByIdQueryHandler(IProductsRepository productsRepository, ICacheService cacheService)
    {
        _productsRepository = productsRepository;
        _cacheService = cacheService;
    }

    public async Task<GetProductByIdResponse> Handle(GetProducyByIdQuery request, CancellationToken cancellationToken)
    {
        Product? result = await _productsRepository.GetById(request.Id);

        if (result == null) 
        {
            return new GetProductByIdResponse(new Product());
        }

        await _cacheService.SetAsync($"product-{result.Id}", result, cancellationToken);

        var response = new GetProductByIdResponse(result);

        return response;
    }
}
