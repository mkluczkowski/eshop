﻿using MediatR;

namespace Shop.Application.Products.GetProductById;

public record GetProducyByIdQuery(Guid Id) : IRequest<GetProductByIdResponse>;
