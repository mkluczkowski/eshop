﻿using Shop.Domain.Products;

namespace Shop.Application.Products.GetProductById;

public record GetProductByIdResponse(Product Product);
