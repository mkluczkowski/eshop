﻿using MediatR;
using Shop.Application.Abstractions.Caching;
using Shop.Domain.Products;
using Shop.Domain.Products.Abstractions;
using System.Collections.Immutable;

namespace Shop.Application.Products.GetAllProducts;

internal class GetAppProductsQueryHandler : IRequestHandler<GetAllProductsQuery, ProductsResponse>
{
    private readonly IProductsRepository _productsRepository;
    private readonly ICacheService _cacheService;

    public GetAppProductsQueryHandler(IProductsRepository productsRepository, ICacheService cacheService)
    {
        _productsRepository = productsRepository;
        _cacheService = cacheService;
    }

    public async Task<ProductsResponse> Handle(GetAllProductsQuery request, CancellationToken cancellationToken)
    {
        IEnumerable<Product> result = await _productsRepository.GetAll();

        foreach (var item in result)
        {
            await _cacheService.SetAsync($"product-{item.Id}", item, cancellationToken);
        }

        var response = new ProductsResponse(result.ToImmutableArray());

        return response;
    }
}
