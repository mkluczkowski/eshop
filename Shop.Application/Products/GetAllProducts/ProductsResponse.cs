﻿using Shop.Domain.Products;
using System.Collections.Immutable;

namespace Shop.Application.Products.GetAllProducts;

public record ProductsResponse(ImmutableArray<Product> Products);
