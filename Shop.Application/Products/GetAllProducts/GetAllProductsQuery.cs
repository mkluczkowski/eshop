﻿using MediatR;

namespace Shop.Application.Products.GetAllProducts;

public record GetAllProductsQuery() : IRequest<ProductsResponse>;
