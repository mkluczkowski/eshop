﻿using MediatR;
using Shop.Application.Abstractions.Caching;
using Shop.Application.Products.Create;
using Shop.Application.Products.Delete;
using Shop.Application.Products.Update;

namespace Shop.Application.Products;

internal class CacheInvalidationProductsHandler : 
    INotificationHandler<ProductCreatedEvent>,
    INotificationHandler<ProductUpdatedEvent>,
    INotificationHandler<ProductDeletedEvent>
{
    private readonly ICacheService _cacheService;

    public CacheInvalidationProductsHandler(ICacheService cacheService)
    {
        _cacheService = cacheService;
    }

    public async Task Handle(ProductCreatedEvent notification, CancellationToken cancellationToken)
    {
        await HandleInternal(notification.Id, cancellationToken);
    }

    public async Task Handle(ProductUpdatedEvent notification, CancellationToken cancellationToken)
    {
        await HandleInternal(notification.Id, cancellationToken);
    }

    public async Task Handle(ProductDeletedEvent notification, CancellationToken cancellationToken)
    {
        await HandleInternal(notification.Id, cancellationToken);
    }

    private async Task HandleInternal(Guid productId, CancellationToken cancellationToken)
    {
        await _cacheService.RemoveBulkByPrefixAsync($"product", cancellationToken);
    }
}
