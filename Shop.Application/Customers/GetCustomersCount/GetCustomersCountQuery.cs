﻿using MediatR;

namespace Shop.Application.Customers.GetCustomersCount;

public record GetCustomersCountQuery() : IRequest<CustomersCountResponse>;
