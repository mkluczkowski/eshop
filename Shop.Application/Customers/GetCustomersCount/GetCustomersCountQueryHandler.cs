﻿using MediatR;
using Shop.Domain.Customers.Abstractions;

namespace Shop.Application.Customers.GetCustomersCount
{
    internal class GetCustomersCountQueryHandler : IRequestHandler<GetCustomersCountQuery, CustomersCountResponse>
    {
        private readonly ICustomersRepository _customersRepository;

        public GetCustomersCountQueryHandler(ICustomersRepository customersRepository)
        {
            _customersRepository = customersRepository;
        }

        public async Task<CustomersCountResponse> Handle(GetCustomersCountQuery request, CancellationToken cancellationToken)
        {
            var result = await _customersRepository.GetCustomerCount();

            return new CustomersCountResponse(result);
        }
    }
}
