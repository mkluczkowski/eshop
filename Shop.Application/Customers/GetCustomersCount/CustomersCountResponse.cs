﻿namespace Shop.Application.Customers.GetCustomersCount;

public record CustomersCountResponse(long Count);
