﻿using MediatR;
using Shop.Application.Abstractions.Authentication;
using Shop.Domain.Customers.Abstractions;

namespace Shop.Application.Customers.Login;

internal sealed class LoginCommandHandler : IRequestHandler<LoginCommand, LoginResponse>
{
    private readonly ICustomersRepository _customerRepository;
    private readonly IJsonWebTokenProvider _tokenProvider;

    public LoginCommandHandler(IJsonWebTokenProvider tokenProvider, ICustomersRepository customerRepository)
    {
        _tokenProvider = tokenProvider;
        _customerRepository = customerRepository;
    }

    public async Task<LoginResponse> Handle(LoginCommand request, CancellationToken cancellationToken)
    {
        var customer = await _customerRepository.GetCustomerByLoginCredentials(request.Email, request.Password);

        if (customer == null)
        {
            return new LoginResponse("The provided credentials are invalid");
        }

        var token = _tokenProvider.Generate(customer);

        var response = new LoginResponse(token);

        return response;
    }
}
