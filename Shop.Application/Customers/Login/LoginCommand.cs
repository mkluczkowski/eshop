﻿using MediatR;

namespace Shop.Application.Customers.Login;

public record LoginCommand(string Email, string Password) : IRequest<LoginResponse>;
