﻿namespace Shop.Application.Customers.Login;

public record LoginRequest(string Email, string Password);
