﻿namespace Shop.Application.Customers.Login;

public record LoginResponse(string Token);
