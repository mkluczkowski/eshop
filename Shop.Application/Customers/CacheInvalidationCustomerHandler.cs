﻿using MediatR;
using Shop.Application.Abstractions.Caching;
using Shop.Application.Customers.Create;
using Shop.Application.Customers.Delete;
using Shop.Application.Customers.Update;

namespace Shop.Application.Customers;

internal class CacheInvalidationCustomerHandler :
    INotificationHandler<CustomerCreatedEvent>,
    INotificationHandler<CustomerUpdatedEvent>,
    INotificationHandler<CustomerDeletedEvent>
{
    private readonly ICacheService _cacheService;

    public CacheInvalidationCustomerHandler(ICacheService cacheService)
    {
        _cacheService = cacheService;
    }

    public async Task Handle(CustomerCreatedEvent notification, CancellationToken cancellationToken)
    {
        await HandleInternal(notification.Id, cancellationToken);
    }

    public async Task Handle(CustomerUpdatedEvent notification, CancellationToken cancellationToken)
    {
        await HandleInternal(notification.Id, cancellationToken);
    }

    public async Task Handle(CustomerDeletedEvent notification, CancellationToken cancellationToken)
    {
        await HandleInternal(notification.Id, cancellationToken);
    }

    private async Task HandleInternal(Guid customerId, CancellationToken cancellationToken)
    {
        await _cacheService.RemoveBulkByPrefixAsync($"customer", cancellationToken);
    }
}
