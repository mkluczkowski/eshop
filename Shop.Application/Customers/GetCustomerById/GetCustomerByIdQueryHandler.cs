﻿using MediatR;
using Shop.Application.Abstractions.Caching;
using Shop.Domain.Customers;
using Shop.Domain.Customers.Abstractions;

namespace Shop.Application.Customers.GetCustomerById;

internal sealed class GetCustomerByIdQueryHandler : IRequestHandler<GetCustomerByIdQuery, CustomerResponse>
{
    private readonly ICustomersRepository _customersRepository;
    private readonly ICacheService _cacheService;

    public GetCustomerByIdQueryHandler(ICustomersRepository customersRepository, ICacheService cacheService)
    {
        _customersRepository = customersRepository;
        _cacheService = cacheService;
    }

    public async Task<CustomerResponse> Handle(GetCustomerByIdQuery request, CancellationToken cancellationToken)
    {
        Customer? customer = await _cacheService.GetAsync<Customer>(request.CustomerId.ToString(), cancellationToken);

        if (customer is not null)
        {
            return new CustomerResponse(customer);
        }

        Customer? result = await _customersRepository.GetById(request.CustomerId);

        if (result is null)
        {
            throw new ArgumentException("Customer not found");
        }

        await _cacheService.SetAsync($"customer-{result.Id}", result, cancellationToken);

        return new CustomerResponse(result);
    }
}
