﻿using Shop.Domain.Customers;

namespace Shop.Application.Customers.GetCustomerById;

public record CustomerResponse(Customer customer);