﻿using MediatR;

namespace Shop.Application.Customers.GetCustomerById;

public record GetCustomerByIdQuery(Guid CustomerId) : IRequest<CustomerResponse>;
