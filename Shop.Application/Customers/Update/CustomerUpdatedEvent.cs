﻿using MediatR;

namespace Shop.Application.Customers.Update;

public record CustomerUpdatedEvent : INotification
{
    public Guid Id { get; init; }
    public string Name { get; init; } = string.Empty;
    public string Email { get; init; } = string.Empty;
}
