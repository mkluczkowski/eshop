﻿using MediatR;
using Shop.Domain.Customers;
using Shop.Domain.Customers.Abstractions;

namespace Shop.Application.Customers.Update;

internal sealed class UpdateCustomerCommandHandler : IRequestHandler<UpdateCustomerCommand, UpdateCustomerResponse>
{
    private readonly ICustomersRepository _customersRepository;
    private readonly IPublisher _publisher;

    public UpdateCustomerCommandHandler(ICustomersRepository customersRepository, IPublisher publisher)
    {
        _customersRepository = customersRepository;
        _publisher = publisher;
    }

    public async Task<UpdateCustomerResponse> Handle(UpdateCustomerCommand request, CancellationToken cancellationToken)
    {
        Customer customer = Customer.Create(request.Id, request.Name, request.Email);

        await _customersRepository.Update(customer);

        await _publisher.Publish(new CustomerUpdatedEvent
        {
            Id = request.Id,
            Name = request.Name,
            Email = request.Email
        },
        cancellationToken);

        return new UpdateCustomerResponse(customer);
    }
}
