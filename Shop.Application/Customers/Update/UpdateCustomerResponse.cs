﻿using Shop.Domain.Customers;

namespace Shop.Application.Customers.Update;

public record UpdateCustomerResponse(Customer Customer);
