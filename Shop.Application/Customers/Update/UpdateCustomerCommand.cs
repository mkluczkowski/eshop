﻿using MediatR;

namespace Shop.Application.Customers.Update;

public record class UpdateCustomerCommand(Guid Id, string Name, string Email) : IRequest<UpdateCustomerResponse>;
