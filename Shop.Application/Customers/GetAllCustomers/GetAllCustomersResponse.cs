﻿using Shop.Domain.Customers;
using System.Collections.Immutable;

namespace Shop.Application.Customers.GetAllCustomers;

public record GetAllCustomersResponse(ImmutableArray<Customer> Customers);
