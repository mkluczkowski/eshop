﻿using MediatR;

namespace Shop.Application.Customers.GetAllCustomers;

public record GetAllCustomersQuery() : IRequest<GetAllCustomersResponse>;
