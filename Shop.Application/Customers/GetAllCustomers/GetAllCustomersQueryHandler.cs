﻿using MediatR;
using Shop.Application.Abstractions.Caching;
using Shop.Domain.Customers;
using Shop.Domain.Customers.Abstractions;
using System.Collections.Immutable;
using System.Runtime.InteropServices;

namespace Shop.Application.Customers.GetAllCustomers;

internal class GetAllCustomersQueryHandler : IRequestHandler<GetAllCustomersQuery, GetAllCustomersResponse>
{
    private readonly ICustomersRepository _customersRepository;
    private readonly ICacheService _cacheService;

    public GetAllCustomersQueryHandler(ICustomersRepository customersRepository, ICacheService cacheService)
    {
        _customersRepository = customersRepository;
        _cacheService = cacheService;
    }

    public async Task<GetAllCustomersResponse> Handle(GetAllCustomersQuery request, CancellationToken cancellationToken)
    {
        IEnumerable<Customer> result = await _customersRepository.GetAll();

        foreach (var item in result)
        {
            await _cacheService.SetAsync($"customer-{item.Id}", item, cancellationToken);
        }

        var response = new GetAllCustomersResponse(result.ToImmutableArray());

        return response;
    }
}
