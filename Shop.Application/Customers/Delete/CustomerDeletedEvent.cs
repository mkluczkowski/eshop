﻿using MediatR;

namespace Shop.Application.Customers.Delete;

public record CustomerDeletedEvent : INotification
{
    public Guid Id { get; init; }
}
