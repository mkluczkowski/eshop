﻿using MediatR;

namespace Shop.Application.Customers.Delete;

public record CustomerDeleteCommand(Guid Id) : IRequest;
