﻿using MediatR;
using Shop.Domain.Customers.Abstractions;

namespace Shop.Application.Customers.Delete
{
    internal sealed class CustomerDeleteCommandHandler : IRequestHandler<CustomerDeleteCommand>
    {
        private readonly ICustomersRepository _customerRepository;

        public CustomerDeleteCommandHandler(ICustomersRepository customerRepository)
        {
            _customerRepository = customerRepository;
        }

        public async Task Handle(CustomerDeleteCommand request, CancellationToken cancellationToken)
        {
            await _customerRepository.Delete(request.Id);
        }
    }
}
