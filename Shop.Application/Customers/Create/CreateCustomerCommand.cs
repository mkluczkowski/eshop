﻿using MediatR;

namespace Shop.Application.Customers.Create;

public sealed record CreateCustomerCommand(string Name, string Email) : IRequest<CreateCustomerResponse>;
