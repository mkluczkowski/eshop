﻿namespace Shop.Application.Customers.Create;

public record CreateCustomerResponse(Guid Id);
