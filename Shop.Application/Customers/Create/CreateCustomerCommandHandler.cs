﻿using MediatR;
using Shop.Domain.Customers;
using Shop.Domain.Customers.Abstractions;

namespace Shop.Application.Customers.Create;

internal sealed class CreateCustomerCommandHandler : IRequestHandler<CreateCustomerCommand, CreateCustomerResponse>
{
    private readonly ICustomersRepository _customersRepository;
    private readonly IPublisher _publisher;

    public CreateCustomerCommandHandler(ICustomersRepository customersRepository, IPublisher publisher)
    {
        _customersRepository = customersRepository;
        _publisher = publisher;
    }

    public async Task<CreateCustomerResponse> Handle(CreateCustomerCommand request, CancellationToken cancellationToken)
    {
        var customer = Customer.Create(Guid.NewGuid(), request.Name, request.Email);

        await _customersRepository.Create(customer);

        var customerCreatedEvent = new CustomerCreatedEvent
        {
            Id = customer.Id,
            Name = customer.Name,
            Email = customer.Email
        };

        await _publisher.Publish(customerCreatedEvent, cancellationToken);

        var response = new CreateCustomerResponse(customer.Id);

        return response;
    }
}
