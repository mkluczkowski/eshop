﻿using Microsoft.Extensions.Caching.Distributed;
using Newtonsoft.Json;
using Shop.Application.Abstractions.Caching;
using System.Collections.Concurrent;

namespace Shop.Infrastructure.Caching;

public class CacheService : ICacheService
{
    private static ConcurrentDictionary<string, bool> CacheKeys = new();

    private readonly IDistributedCache _distributedCache;

    public CacheService(IDistributedCache distributedCache)
    {
        _distributedCache = distributedCache;
    }

    public async Task<T?> GetAsync<T>(string cacheKey, CancellationToken cancellationToken = default) where T : class
    {
        string? cacheValue = await _distributedCache.GetStringAsync(cacheKey, cancellationToken);

        if (cacheValue is null)
        {
            return null;
        }

        T? value = JsonConvert.DeserializeObject<T>(cacheValue);

        return value;
    }

    public async Task SetAsync<T>(string cacheKey, T value, CancellationToken cancellationToken = default) where T : class
    {
        var cacheValue = JsonConvert.SerializeObject(value);

        await _distributedCache.SetStringAsync(cacheKey, cacheValue, cancellationToken);

        CacheKeys.TryAdd(cacheKey, true);
    }

    public async Task RemoveAsync(string cacheKey, CancellationToken cancellationToken = default)
    {
        await _distributedCache.RemoveAsync(cacheKey, cancellationToken);

        CacheKeys.TryRemove(cacheKey, out bool _);
    }

    public async Task RemoveBulkByPrefixAsync(string prefixKey, CancellationToken cancellationToken = default)
    {
        IEnumerable<Task> tasks = CacheKeys.Keys
            .Where(key => key.StartsWith(prefixKey))
            .Select(key => RemoveAsync(key, cancellationToken));

        await Task.WhenAll(tasks);
    }
}
