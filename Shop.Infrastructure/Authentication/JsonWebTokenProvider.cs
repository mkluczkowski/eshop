﻿using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using Shop.Application.Abstractions.Authentication;
using Shop.Domain.Customers;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace Shop.Infrastructure.Authentication;

public sealed class JsonWebTokenProvider : IJsonWebTokenProvider
{
    private readonly JsonWebTokenOptions _options;

    public JsonWebTokenProvider(IOptions<JsonWebTokenOptions> options)
    {
        _options = options.Value;
    }

    public string Generate(Customer customer)
    {
        var claims = new Claim[]
        {
            new(JwtRegisteredClaimNames.Sub, customer.Id.ToString()),
            new(JwtRegisteredClaimNames.Name, customer.Name),
            new(JwtRegisteredClaimNames.Email, customer.Email),
            new(JwtRegisteredClaimNames.AuthTime, DateTime.UtcNow.AddHours(2).ToString())
        };

        var signingCredentials = new SigningCredentials(
            new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_options.SecretKey!)),
                SecurityAlgorithms.HmacSha256);

        var token = new JwtSecurityToken(
            issuer: _options.Issuer,
            audience: _options.Audience,
            claims: claims,
            expires: DateTime.Now.AddHours(2),
            signingCredentials: signingCredentials);

        var tokenValue = new JwtSecurityTokenHandler().WriteToken(token);

        return tokenValue;
    }
}
