﻿using System.Reflection;

namespace Shop.Infrastructure;

public static class AssemblyReference
{
    public static Assembly Assembly = typeof(AssemblyReference).Assembly;
}
