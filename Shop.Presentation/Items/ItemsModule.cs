﻿using Carter;
using MediatR;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;

namespace Shop.Presentation.Items;

public class ItemsModule : ICarterModule
{
    public void AddRoutes(IEndpointRouteBuilder app)
    {
        RouteGroupBuilder group = app.MapGroup("/items")
            .WithGroupName("Items");

        group.MapGet("", ([FromServices] ISender sender) => { })
        .RequireAuthorization();

        group.MapGet("/{id}", (Guid id, [FromServices] ISender sender) => { })
        .RequireAuthorization();

        group.MapPost("", async (Guid customerId, [FromServices] ISender sender) => { })
        .RequireAuthorization();

        group.MapPut("", async (Guid orderId, Guid customerId, string email, [FromServices] ISender sender) => { })
        .RequireAuthorization();

        group.MapDelete("", async (Guid id, [FromServices] ISender sender) => { })
        .RequireAuthorization();
    }
}
