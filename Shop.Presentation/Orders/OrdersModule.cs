﻿using Carter;
using MediatR;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using Shop.Application.Orders.GetOrders;

namespace Shop.Presentation.Orders;

public class OrdersModule : ICarterModule
{
    public void AddRoutes(IEndpointRouteBuilder app)
    {
        var group = app.MapGroup("/orders")
            .WithGroupName("Orders");

        group.MapGet("", async ([FromServices] ISender sender) => 
        {
            var query = new GetOrdersQuery();

            return Results.Ok(await sender.Send(query));
        })
        .RequireAuthorization();

        group.MapGet("/{id}", (Guid id, [FromServices] ISender sender) => { })
        .RequireAuthorization();

        group.MapPost("", async (Guid customerId, [FromServices] ISender sender) => { })
        .RequireAuthorization();

        group.MapPut("", async (Guid orderId, Guid customerId, string email, [FromServices] ISender sender) => { })
        .RequireAuthorization();

        group.MapDelete("", async (Guid id, [FromServices] ISender sender) => { })
        .RequireAuthorization();
    }
}
