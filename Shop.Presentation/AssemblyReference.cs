﻿using System.Reflection;

namespace Shop.Presentation;

public static class AssemblyReference
{
    public static Assembly Assembly = typeof(AssemblyReference).Assembly;
}
