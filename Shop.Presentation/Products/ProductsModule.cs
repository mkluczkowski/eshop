﻿using Carter;
using MediatR;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using Shop.Application.Products.Create;
using Shop.Application.Products.Delete;
using Shop.Application.Products.GetAllProducts;
using Shop.Application.Products.GetProductById;
using Shop.Application.Products.Update;

namespace Shop.Presentation.Products;

public class ProductsModule : ICarterModule
{
    public void AddRoutes(IEndpointRouteBuilder app)
    {
        var group = app.MapGroup("/products")
            .WithGroupName("Product");

        group.MapGet("", async ([FromServices] ISender sender) =>
        {
            var query = new GetAllProductsQuery();

            return Results.Ok(await sender.Send(query));
        });

        group.MapGet("/{id}", async ([FromRoute] Guid id, [FromServices] ISender sender) =>
        {
            var query = new GetProducyByIdQuery(id);

            return Results.Ok(await sender.Send(query));
        });

        group.MapPost("", async (
            [FromBody] CreateProductRequest request,
            [FromServices] ISender sender) =>
        {
            var command = new CreateProductCommand(request.Name, request.Sku, request.Currency, request.Amount);

            var result = await sender.Send(command);

            return Results.Created("", result);
        });

        group.MapPut("", async (
            [FromBody] UpdateProductRequest request,
            [FromServices] ISender sender) =>
        {
            var command = new UpdateProductCommand(request.Id, request.Name, request.Sku, request.Currency, request.Amount);

            await sender.Send(command);

            return Results.NoContent();
        });

        group.MapDelete("/{id}", async ([FromRoute] Guid Id, [FromServices] ISender sender) =>
        {
            var command = new DeleteProductCommand(Id);

            await sender.Send(command);

            return Results.NoContent();
        });
    }
}
