﻿using Carter;
using MediatR;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using Shop.Application.Customers.Create;
using Shop.Application.Customers.Delete;
using Shop.Application.Customers.GetAllCustomers;
using Shop.Application.Customers.GetCustomerById;
using Shop.Application.Customers.GetCustomersCount;
using Shop.Application.Customers.Login;
using Shop.Application.Customers.Update;

namespace Shop.Presentation.Customers;

public class CustomersModule : ICarterModule
{
    public void AddRoutes(IEndpointRouteBuilder app)
    {
        RouteGroupBuilder group = app.MapGroup("/customers")
            .WithGroupName("Customer");

        group.MapGet("", async ([FromServices] ISender sender) =>
        {
            var query = new GetAllCustomersQuery();

            return Results.Ok(await sender.Send(query));
        })
        .RequireAuthorization();

        group.MapGet("/{id}", async ([FromRoute] Guid id, [FromServices] ISender sender) =>
        {
            var query = new GetCustomerByIdQuery(id);

            return Results.Ok(await sender.Send(query));
        })
        .RequireAuthorization();

        group.MapGet("count", async ([FromServices] ISender sender) =>
        {
            var query = new GetCustomersCountQuery();

            return Results.Ok(await sender.Send(query));
        });

        group.MapPost("login", async ([FromBody] LoginRequest request, [FromServices] ISender sender) =>
        {
            var command = new LoginCommand(request.Email, request.Password);

            var result = await sender.Send(command);

            return Results.Ok(result);
        })
        .AllowAnonymous();

        group.MapPost("", async (string name, string email, [FromServices] ISender sender) =>
        {
            var command = new CreateCustomerCommand(name, email);

            var result = await sender.Send(command);

            return Results.Ok(result);
        })
        .RequireAuthorization();

        group.MapPut("", async (Guid id, string name, string email, [FromServices] ISender sender) =>
        {
            var command = new UpdateCustomerCommand(id, name, email);

            var result = await sender.Send(command);

            return Results.Ok(result);
        })
        .RequireAuthorization();

        group.MapDelete("", async (Guid id, [FromServices] ISender sender) =>
        {
            var command = new CustomerDeleteCommand(id);

            await sender.Send(command);

            return Results.NoContent();
        })
        .RequireAuthorization();
    }
}
