﻿using Microsoft.AspNetCore.Mvc;
using System.Net;
using System.Text.Json;
using ILogger = Serilog.ILogger;

namespace Shop.Middlewares;

public class GlobalExceptionHandlingMiddleware : IMiddleware
{
	private readonly ILogger _logger;

    public GlobalExceptionHandlingMiddleware(ILogger logger)
    {
        _logger = logger;
    }

    public async Task InvokeAsync(HttpContext context, RequestDelegate next)
    {
		try
		{
			await next(context);
		}
		catch (Exception e)
		{
			_logger.Error(e.Message);

			context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;

			ProblemDetails problem = new()
			{
				Status = (int)HttpStatusCode.InternalServerError,
				Type = "Server error",
				Title = "Server error",
				Detail = "Internal server error"
			};

			var json = JsonSerializer.Serialize(problem);

			await context.Response.WriteAsync(json);

			context.Response.ContentType = "application/json";
		}
    }
}
