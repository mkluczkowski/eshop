﻿using Microsoft.Extensions.Options;
using Shop.Infrastructure.Authentication;

namespace Shop.OptionsSetup;

public class JsonWebTokenOptionsSetup : IConfigureOptions<JsonWebTokenOptions>
{
    private const string SectionName = "JsonWebToken";
    private readonly IConfiguration _configuration;

    public JsonWebTokenOptionsSetup(IConfiguration configuration)
    {
        _configuration = configuration;
    }

    public void Configure(JsonWebTokenOptions options)
    {
        _configuration.GetSection(SectionName).Bind(options);
    }
}
