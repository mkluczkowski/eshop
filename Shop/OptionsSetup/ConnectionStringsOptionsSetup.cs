﻿using Microsoft.Extensions.Options;
using Shop.Persistence;

namespace Shop.OptionsSetup;

public class ConnectionStringsOptionsSetup : IConfigureOptions<ConnectionStringsOptions>
{
    private const string SectionName = "ConnectionStrings";
    private readonly IConfiguration _configuration;

    public ConnectionStringsOptionsSetup(IConfiguration configuration)
    {
        _configuration = configuration;
    }

    public void Configure(ConnectionStringsOptions options)
    {
        _configuration.GetSection(SectionName).Bind(options);
    }
}
