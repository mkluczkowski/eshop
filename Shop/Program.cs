using Carter;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Scrutor;
using Serilog;
using Serilog.Events;
using Shop.Application.Abstractions.Caching;
using Shop.DependencyInjection;
using Shop.Infrastructure.Caching;
using Shop.Middlewares;
using Shop.OptionsSetup;

var builder = WebApplication.CreateBuilder(args);

builder.Services
    .Scan(selector =>
        selector.FromAssemblies(
            Shop.Persistence.AssemblyReference.Assembly,
            Shop.Infrastructure.AssemblyReference.Assembly
            )
        .AddClasses(false)
        .UsingRegistrationStrategy(RegistrationStrategy.Skip)
        .AsImplementedInterfaces()
        .WithScopedLifetime());

builder.Services
    .AddMediatR(comfig =>
    {
        comfig.RegisterServicesFromAssembly(Shop.Application.AssemblyReference.Assembly);
    });

builder.Services.AddEndpointsApiExplorer();

builder.Services.AddCarter();

builder.Services.AddSwagger();

builder.Services.AddDistributedMemoryCache();
builder.Services.AddSingleton<ICacheService, CacheService>();

builder.Services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
    .AddJwtBearer();

builder.Services.AddAuthorization();

builder.Services.ConfigureOptions<ConnectionStringsOptionsSetup>();
builder.Services.ConfigureOptions<JsonWebTokenBearerOptionsSetup>();
builder.Services.ConfigureOptions<JsonWebTokenOptionsSetup>();

builder.Services.AddTransient<GlobalExceptionHandlingMiddleware>();

builder.Logging.ClearProviders();
builder.Host.UseSerilog((hostContext, services, configuration) =>
{
    configuration
    .MinimumLevel.Error()
    .WriteTo.Console()
    .WriteTo.File(
        path: "logs/api-log-.txt",
        outputTemplate: "{Timestamp:yyyy-MM-dd HH:mm:ss.fff zzz} [{Level:u3}] {Message:lj}{NewLine}{Exception}",
        rollingInterval: RollingInterval.Day)
    .WriteTo.File(
        path: "logs/debug-log-.txt",
        outputTemplate: "{Timestamp:yyyy-MM-dd HH:mm:ss.fff zzz} [{Level:u3}] {Message:lj}{NewLine}{Exception}",
        rollingInterval: RollingInterval.Day,
        restrictedToMinimumLevel: LogEventLevel.Information)
    .Enrich.FromLogContext();
});

var app = builder.Build();

app.UseSwagger();
app.UseSwaggerUI();

app.MapCarter();

app.UseHttpsRedirection();

app.UseAuthentication();

app.UseAuthorization();

app.UseMiddleware<GlobalExceptionHandlingMiddleware>();

app.Run();