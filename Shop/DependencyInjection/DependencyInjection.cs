﻿using Microsoft.OpenApi.Models;

namespace Shop.DependencyInjection;

public static class DependencyInjection
{
    public static IServiceCollection AddSwagger(this IServiceCollection services)
    {
        services.AddSwaggerGen(options =>
        {
            options.SwaggerDoc("v1", new OpenApiInfo
            {
                Version = "v1",
                Title = "Test API",
                Description = "Simple CRUD test API representing Shop",
                Contact = new OpenApiContact
                {
                    Email = "kluczkowskimicha@gmail.com",
                    Name = "Michał Kluczkowski",
                    Url = new Uri("https://gitlab.com/mkluczkowski")
                }
            });

            options.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
            {
                Name = "Authorization",
                Description = "Insert JWT with Bearer",
                Type = SecuritySchemeType.ApiKey,
                BearerFormat = "JWT",
                In = ParameterLocation.Header,
            });

            options.AddSecurityRequirement(new OpenApiSecurityRequirement
            {
                {
                     new OpenApiSecurityScheme
                     {
                         Reference = new OpenApiReference
                         {
                             Type = ReferenceType.SecurityScheme,
                             Id = "Bearer"
                         }
                     },
                     Array.Empty<string>()
                }
            });

            options.TagActionsBy(tagSelector => new[] { tagSelector.GroupName });
            options.DocInclusionPredicate((name, apiDescription) => true);
        });

        return services;
    }
}
